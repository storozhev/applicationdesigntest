package orders

import (
	"applicationDesignTest/internal/entities"
	"applicationDesignTest/internal/repository/orders"
	"applicationDesignTest/internal/types"
	"context"
	"errors"
)

var (
	ErrOrdersConflicted = errors.New("orders conflicted")
	ErrNoOrdersFound    = errors.New("no orders found")
)

type CreateRequest struct {
	Email    string
	RoomType entities.RoomType
	DateFrom types.Time
	DateTo   types.Time
}

type FindRequest struct {
	Email string
}

type Service struct {
	repo orders.Repository
}

func NewService(repo orders.Repository) *Service {
	return &Service{repo: repo}
}

func (s *Service) Get(ctx context.Context, request FindRequest) ([]entities.Order, error) {
	foundOrders, err := s.repo.Find(ctx, orders.FindRequest{
		Email: request.Email,
	})
	if errors.Is(err, orders.ErrNoOrdersFound) {
		return nil, ErrNoOrdersFound
	}

	return foundOrders, nil
}

func (s *Service) Create(ctx context.Context, request CreateRequest) error {
	// todo: firstly we must check if there are awailable rooms

	err := s.repo.Store(ctx, entities.Order{
		UserEmail: request.Email,
		RoomType:  request.RoomType,
		From:      request.DateFrom,
		To:        request.DateTo,
	})

	if errors.Is(err, orders.ErrOrderTimeRangeIntersection) {
		return ErrOrdersConflicted
	}

	return err
}
