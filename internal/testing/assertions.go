package testing

import (
	"reflect"
	"strings"
	"testing"
)

const (
	expectedGotExpression    = "\nexpected:%#v\nbut\ngot:%#v\n"
	doesNotContainExpression = "\nexpression:%q does not contain %q\n"
)

type ErrorAssertionFunc = func(tb testing.TB, err error, args ...any)

func Equal(tb testing.TB, expected, got any) {
	if !reflect.DeepEqual(expected, got) {
		tb.Errorf(expectedGotExpression, expected, got)
	}
}

func ErrorContains(tb testing.TB, err error, target string) {
	if !strings.Contains(err.Error(), target) {
		tb.Errorf(doesNotContainExpression, err.Error(), target)
	}
}

func NoError(tb testing.TB, err error, args ...any) {
	if err != nil {
		tb.Errorf(expectedGotExpression, nil, err)
	}
}
