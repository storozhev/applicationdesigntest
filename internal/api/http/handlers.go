package http

import (
	"applicationDesignTest/internal/service/orders"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
)

func (sm *serveMux) makeOrder(w http.ResponseWriter, r *http.Request) {
	var requestData makeOrderRequestModel
	if err := json.NewDecoder(r.Body).Decode(&requestData); err != nil {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	if err := requestData.Valid(); err != nil {
		http.Error(w,
			fmt.Sprintf("%s: %s", http.StatusText(http.StatusBadRequest), err.Error()),
			http.StatusBadRequest,
		)
		return
	}

	if err := sm.orderService.Create(r.Context(), orders.CreateRequest{
		RoomType: requestData.RoomType,
		Email:    requestData.Email,
		DateFrom: requestData.DateFrom,
		DateTo:   requestData.DateTo,
	}); err != nil {
		if errors.Is(err, orders.ErrOrdersConflicted) {
			http.Error(w, http.StatusText(http.StatusConflict), http.StatusConflict)
		} else {
			w.WriteHeader(http.StatusInternalServerError)
			sm.logger.Errorf("failed to create order: %s", err)
		}

		return
	}

	w.WriteHeader(http.StatusCreated)
	sm.logger.Infof("Method makeOrder was successfully done")
}

func (sm *serveMux) getOrders(w http.ResponseWriter, r *http.Request) {
	email := r.URL.Query().Get("email")
	if len(email) == 0 {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	ordersFound, err := sm.orderService.Get(r.Context(), orders.FindRequest{
		Email: email,
	})

	if err != nil {
		if errors.Is(err, orders.ErrNoOrdersFound) {
			http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
		} else {
			w.WriteHeader(http.StatusInternalServerError)
			sm.logger.Errorf("failed to create order: %s", err)
		}

		return
	}

	if err := json.NewEncoder(w).Encode(ordersFound); err != nil {
		sm.logger.Errorf("error in getOrders method: %s", err.Error())
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	sm.logger.Infof("Method getOrders was successfully done")
}
