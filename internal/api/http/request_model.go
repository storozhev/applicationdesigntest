package http

import (
	"applicationDesignTest/internal/entities"
	"applicationDesignTest/internal/types"
	"fmt"
	"net/mail"
	"time"
)

type makeOrderRequestModel struct {
	Email    string            `json:"email"`
	RoomType entities.RoomType `json:"room_type"`
	DateFrom types.Time        `json:"date_from"`
	DateTo   types.Time        `json:"date_to"`
}

func (r *makeOrderRequestModel) Valid() error {
	if len(r.Email) == 0 {
		return fmt.Errorf("field `email` must not be empty")
	}

	if _, err := mail.ParseAddress(r.Email); err != nil {
		return fmt.Errorf("field `email` %w", err)
	}

	if err := r.RoomType.Valid(); err != nil {
		return fmt.Errorf("field `room_type` %w", err)
	}

	if r.DateFrom.Before(time.Now()) {
		return fmt.Errorf("field `date_from` must not be in the past")
	}

	if r.DateTo.Before(time.Now()) {
		return fmt.Errorf("field `date_to` must not be in the past")
	}

	if r.DateTo.Before(r.DateFrom.Time) {
		return fmt.Errorf("field `date_to` must not be less than `date_from`")
	}

	return nil
}
