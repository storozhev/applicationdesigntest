package http

import (
	"applicationDesignTest/internal/log"
	"applicationDesignTest/internal/service/orders"
	"net/http"
)

type serveMux struct {
	orderService *orders.Service
	logger       log.Logger
	mux          *http.ServeMux
}

func NewServeMux(logger log.Logger, orderService *orders.Service) *serveMux {
	return &serveMux{
		orderService: orderService,
		logger:       logger,
		mux:          http.NewServeMux(),
	}
}

func (sm *serveMux) InitHandlers() {
	sm.mux.HandleFunc("/order", sm.makeOrder)
	sm.mux.HandleFunc("/orders", sm.getOrders)
}

func (sm *serveMux) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	sm.mux.ServeHTTP(w, r)
}
