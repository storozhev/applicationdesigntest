package http

import (
	"applicationDesignTest/internal/log"
	assert "applicationDesignTest/internal/testing"
	"bytes"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestServeMuxMakeOrder(t *testing.T) {
	testCases := map[string]struct {
		requestBody            string
		expectedHttpStatusCode int
		expectedResponseBody   string
	}{
		"invalid json body": {
			requestBody:            "this is not json",
			expectedHttpStatusCode: http.StatusBadRequest,
			expectedResponseBody:   http.StatusText(http.StatusBadRequest),
		},
		"empty email": {
			requestBody:            `{"email":""}`,
			expectedHttpStatusCode: http.StatusBadRequest,
			expectedResponseBody: fmt.Sprintf(
				"%s: %s",
				http.StatusText(http.StatusBadRequest),
				"field `email` must not be empty",
			),
		},
		"invalid email": {
			requestBody:            `{"email":"bla#gmail.com"}`, // # instead of @
			expectedHttpStatusCode: http.StatusBadRequest,
			expectedResponseBody: fmt.Sprintf(
				"%s: %s",
				http.StatusText(http.StatusBadRequest),
				"field `email` mail: missing '@' or angle-addr",
			),
		},
		"invalid room_type": {
			requestBody:            `{"email":"bla@gmail.com", "room_type":66}`,
			expectedHttpStatusCode: http.StatusBadRequest,
			expectedResponseBody: fmt.Sprintf(
				"%s: %s",
				http.StatusText(http.StatusBadRequest),
				"field `room_type` invalid value",
			),
		},
		"invalid date_from": {
			requestBody: `
			{"email":"foo@bar.com", "room_type":1, "date_from":"2022-01-01"}`, // past
			expectedHttpStatusCode: http.StatusBadRequest,
			expectedResponseBody: fmt.Sprintf(
				"%s: %s",
				http.StatusText(http.StatusBadRequest),
				"field `date_from` must not be in the past",
			),
		},
		"invalid date_to": {
			requestBody: `
			{"email":"foo@bar.com", "room_type":1, "date_from":"2024-02-01", "date_to":"2022-02-01"}`, // past
			expectedHttpStatusCode: http.StatusBadRequest,
			expectedResponseBody: fmt.Sprintf(
				"%s: %s",
				http.StatusText(http.StatusBadRequest),
				"field `date_to` must not be in the past",
			),
		},
		"date_to is less date_from": {
			requestBody: `
			{"email":"foo@bar.com", "room_type":1, "date_from":"2025-02-01", "date_to":"2024-02-01"}`,
			expectedHttpStatusCode: http.StatusBadRequest,
			expectedResponseBody: fmt.Sprintf(
				"%s: %s",
				http.StatusText(http.StatusBadRequest),
				"field `date_to` must not be less than `date_from`",
			),
		},
	}

	for tcName, tc := range testCases {
		t.Run(tcName, func(t *testing.T) {
			w := httptest.NewRecorder()
			r := httptest.NewRequest(
				http.MethodPost,
				"/test",
				bytes.NewBufferString(tc.requestBody),
			)
			(&serveMux{logger: log.MockedLogger{}}).makeOrder(w, r)

			gotBody, _ := io.ReadAll(w.Result().Body)

			assert.Equal(t, tc.expectedHttpStatusCode, w.Result().StatusCode)
			assert.Equal(t, tc.expectedResponseBody, strings.TrimSuffix(string(gotBody), "\n"))
		})
	}
}

func TestServeMuxGetOrders(t *testing.T) {
	// sm := serveMux{logger: log.MockedLogger{}}

	// sm.getOrders()
}
