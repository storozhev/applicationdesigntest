package log

type MockedLogger struct{}

func (MockedLogger) Debugf(format string, args ...any) {}
func (MockedLogger) Infof(format string, args ...any)  {}
func (MockedLogger) Warnf(format string, args ...any)  {}
func (MockedLogger) Errorf(format string, args ...any) {}
func (MockedLogger) Fatalf(format string, args ...any) {}
