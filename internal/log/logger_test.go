package log

import (
	assert "applicationDesignTest/internal/testing"
	"bytes"
	"io"
	"log"
	"testing"
)

const testLoggerPrefix = "prefix"

func TestBuildMessage(t *testing.T) {
	format := "foo"

	got := buildMessage(bytes.NewBuffer(nil), format, InfoLevel)

	assert.Equal(t, "[info] foo", got)
}

func TestLogger_Debugf(t *testing.T) {
	var output bytes.Buffer
	var logger = prepareLogger(&output)

	logger.Debugf("foo:%s", "bar")

	assert.Equal(t,
		testLoggerPrefix+" [debug] foo:bar\n",
		output.String(),
	)
}

func TestLogger_Infof(t *testing.T) {
	var output bytes.Buffer
	var logger = prepareLogger(&output)

	logger.Infof("foo:%s", "bar")

	assert.Equal(t,
		testLoggerPrefix+" [info] foo:bar\n",
		output.String(),
	)
}

func TestLogger_Warnf(t *testing.T) {
	var output bytes.Buffer
	var logger = prepareLogger(&output)

	logger.Warnf("foo:%s", "bar")

	assert.Equal(t,
		testLoggerPrefix+" [warn] foo:bar\n",
		output.String(),
	)
}

func TestLogger_Errorf(t *testing.T) {
	var output bytes.Buffer
	var logger = prepareLogger(&output)

	logger.Errorf("foo:%s", "bar")

	assert.Equal(t,
		testLoggerPrefix+" [error] foo:bar\n",
		output.String(),
	)
}

func prepareLogger(output io.Writer) *logger {
	return &logger{
		msgBuffer: bytes.NewBuffer(nil),
		stdlog:    log.New(output, testLoggerPrefix+" ", 0),
	}
}
