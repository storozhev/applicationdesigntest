package log

import (
	"bytes"
	"io"
	stdLog "log"
)

type Logger interface {
	Debugf(format string, args ...any)
	Infof(format string, args ...any)
	Warnf(format string, args ...any)
	Errorf(format string, args ...any)
	Fatalf(format string, args ...any)
}

type LogLevel int8

const (
	// maybe tracing will be required in the future
	_ LogLevel = iota - 1
	DebugLevel
	InfoLevel
	WarnLevel
	ErrorLevel
	FatalLevel
)

var logLevels = map[LogLevel]string{
	DebugLevel: "debug",
	InfoLevel:  "info",
	WarnLevel:  "warn",
	ErrorLevel: "error",
	FatalLevel: "fatal",
}

func (ll LogLevel) String() string {
	return logLevels[ll]
}

type logger struct {
	msgBuffer *bytes.Buffer
	stdlog    *stdLog.Logger
}

func New(output io.Writer, prefix string) *logger {
	var buf [1024]byte

	return &logger{
		msgBuffer: bytes.NewBuffer(buf[:]),
		stdlog:    stdLog.New(output, prefix, stdLog.Ldate|stdLog.Ltime),
	}
}

func (l *logger) Debugf(format string, args ...any) {
	l.stdlog.Printf(
		buildMessage(l.msgBuffer, format, DebugLevel),
		args...,
	)
}

func (l *logger) Infof(format string, args ...any) {
	l.stdlog.Printf(
		buildMessage(l.msgBuffer, format, InfoLevel),
		args...,
	)
}

func (l *logger) Warnf(format string, args ...any) {
	l.stdlog.Printf(
		buildMessage(l.msgBuffer, format, WarnLevel),
		args...,
	)
}

func (l *logger) Errorf(format string, args ...any) {
	l.stdlog.Printf(
		buildMessage(l.msgBuffer, format, ErrorLevel),
		args...,
	)
}

func (l *logger) Fatalf(format string, args ...any) {
	l.stdlog.Fatalf(
		buildMessage(l.msgBuffer, format, FatalLevel),
		args...,
	)
}

func buildMessage(buf *bytes.Buffer, format string, level LogLevel) string {
	buf.Reset()
	buf.WriteString("[")
	buf.WriteString(level.String())
	buf.WriteString("] ")
	buf.WriteString(format)

	return buf.String()
}
