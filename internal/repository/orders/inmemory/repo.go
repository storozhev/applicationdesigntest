package inmemory

import (
	"applicationDesignTest/internal/entities"
	"applicationDesignTest/internal/repository/orders"
	"context"
)

type repo struct {
	byEmailMap map[string][]*entities.Order
	genID      IDGenerator
}

func New(idGen IDGenerator) *repo {
	return &repo{
		byEmailMap: make(map[string][]*entities.Order),
		genID:      idGen,
	}
}

func (r *repo) Store(ctx context.Context, order entities.Order) error {
	storedOrders := r.byEmailMap[order.UserEmail]
	for _, stored := range storedOrders {
		if (order.From.After(stored.From.Time) && order.From.Before(stored.To.Time)) ||
			(order.To.After(stored.From.Time) && order.To.Before(stored.To.Time)) {
			return orders.ErrOrderTimeRangeIntersection
		}
	}

	order.ID = r.genID()
	storedOrders = append(storedOrders, &order)
	r.byEmailMap[order.UserEmail] = storedOrders

	return nil
}

func (r *repo) Find(ctx context.Context, request orders.FindRequest) ([]entities.Order, error) {
	storedOrders, ok := r.byEmailMap[request.Email]
	if !ok {
		return nil, orders.ErrNoOrdersFound
	}

	// must not allow to mutate orders outside so make copies...
	ordersToBeReturned := make([]entities.Order, len(storedOrders))
	for i, storedOrder := range storedOrders {
		ordersToBeReturned[i] = *storedOrder
	}

	return ordersToBeReturned, nil
}

type IDGenerator func() int

func SimpleIDGenerator() func() int {
	var i int
	return func() int {
		i++
		return i
	}
}
