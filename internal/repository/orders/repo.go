package orders

import (
	"applicationDesignTest/internal/entities"
	"context"
	"errors"
)

var (
	ErrOrderTimeRangeIntersection = errors.New("orders time ranges intersected")
	ErrNoOrdersFound              = errors.New("no orders found")
)

type FindRequest struct {
	Email string
}

type Repository interface {
	Find(context.Context, FindRequest) ([]entities.Order, error)
	Store(context.Context, entities.Order) error
}
