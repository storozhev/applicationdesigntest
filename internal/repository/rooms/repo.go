package rooms

import (
	"applicationDesignTest/internal/entities"
	"context"
)

type CreateRequest struct {
	Name string
	Type entities.RoomType
}

type FindRequest struct {
	Type entities.RoomType
}

type Repository interface {
	Find(context.Context, FindRequest) ([]entities.Room, error)
	Store(context.Context, entities.Room) error
}
