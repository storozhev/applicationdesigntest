package types

import (
	"fmt"
	"strings"
	"time"
)

type Time struct {
	time.Time
}

func (t *Time) UnmarshalJSON(b []byte) error {
	raw := strings.Trim(string(b), "\"")
	r, err := time.Parse("2006-01-02", raw)
	if err != nil {
		return fmt.Errorf("failed to parse time: %w", err)
	}
	*t = Time{r}

	return nil
}
