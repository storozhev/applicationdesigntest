package types

import (
	assert "applicationDesignTest/internal/testing"
	"encoding/json"
	"testing"
	"time"
)

func TestTimeUnmarshalJSON(t *testing.T) {
	testCases := map[string]struct {
		input              string
		errorAssertionFunc assert.ErrorAssertionFunc
		expectedResult     Time
	}{
		"invalid value": {
			input:          `{"time":"invalid"}`,
			expectedResult: Time{time.Time{}},
			errorAssertionFunc: func(tb testing.TB, err error, args ...any) {
				assert.ErrorContains(tb, err, "failed to parse time")
			},
		},
		"valid value": {
			input:              `{"time":"2023-02-03"}`,
			expectedResult:     Time{time.Date(2023, 2, 3, 0, 0, 0, 0, time.UTC)},
			errorAssertionFunc: assert.NoError,
		},
	}

	for tcName, tc := range testCases {
		t.Run(tcName, func(t *testing.T) {
			var gotResult struct{ Time Time }

			tc.errorAssertionFunc(t, json.Unmarshal([]byte(tc.input), &gotResult))
			assert.Equal(t, tc.expectedResult, gotResult.Time)
		})
	}
}
