package entities

import "applicationDesignTest/internal/types"

type Order struct {
	ID        int
	RoomType  RoomType
	UserEmail string
	From      types.Time
	To        types.Time
}
