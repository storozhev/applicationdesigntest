package entities

import (
	"errors"
)

const (
	Econom RoomType = iota + 1
	Standart
	Lux
)

var ErrInvalidRoomType = errors.New("invalid value")

var roomTypes = map[RoomType]string{
	Econom:   "econom",
	Standart: "standart",
	Lux:      "lux",
}

type RoomType int

func (rt RoomType) String() string {
	return roomTypes[rt]
}

func (rt RoomType) Valid() error {
	if _, ok := roomTypes[rt]; !ok {
		return ErrInvalidRoomType
	}

	return nil
}

type Room struct {
	ID   int
	Name string
	Type RoomType
}
