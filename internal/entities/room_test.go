package entities

import (
	assert "applicationDesignTest/internal/testing"
	"testing"
)

func TestRoomTypeValid(t *testing.T) {
	testCases := map[string]struct {
		value         RoomType
		expectedError error
	}{
		"negative value": {
			value:         0,
			expectedError: ErrInvalidRoomType,
		},
		"too big value": {
			value:         RoomType(len(roomTypes) + 1),
			expectedError: ErrInvalidRoomType,
		},
		"valid value": {
			value: Lux,
		},
	}

	for tcName, tc := range testCases {
		t.Run(tcName, func(t *testing.T) {
			assert.Equal(t, tc.expectedError, tc.value.Valid())
		})
	}
}
