package config

import (
	assert "applicationDesignTest/internal/testing"
	"bytes"
	"testing"
)

func TestFromJSON(t *testing.T) {
	testCases := map[string]struct {
		input              string
		expectedResult     *Config
		errorAssertionFunc assert.ErrorAssertionFunc
	}{
		"invalid json": {
			input: "{]",
			errorAssertionFunc: func(tb testing.TB, err error, args ...any) {
				assert.ErrorContains(tb, err, "failed to parse config")
			},
		},
		"valid json": {
			input: `{"api": {"http": {"port": 666}}}`,
			expectedResult: &Config{
				Api: Api{Http{Port: 666}},
			},
			errorAssertionFunc: assert.NoError,
		},
	}

	for tcName, tc := range testCases {
		t.Run(tcName, func(t *testing.T) {
			gotResult, gotError := FromJSON(bytes.NewBufferString(tc.input))

			assert.Equal(t, tc.expectedResult, gotResult)
			tc.errorAssertionFunc(t, gotError)
		})
	}
}
