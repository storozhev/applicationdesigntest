package config

import (
	"encoding/json"
	"fmt"
	"io"
)

type Http struct {
	Port int `json:"port"`
}

type Api struct {
	Http Http `json:"http"`
}

type Config struct {
	Api Api `json:"api"`
}

func FromJSON(r io.Reader) (*Config, error) {
	var conf Config
	if err := json.NewDecoder(r).Decode(&conf); err != nil {
		return nil, fmt.Errorf("failed to parse config: %w", err)
	}

	return &conf, nil
}
