package main

import (
	"applicationDesignTest/cmd"
	"applicationDesignTest/internal/config"
	"applicationDesignTest/internal/log"
	"applicationDesignTest/internal/repository/orders/inmemory"
	ordersSvc "applicationDesignTest/internal/service/orders"
	"flag"
	"os"
)

var configFile string

func init() {
	flag.StringVar(&configFile, "c", "config.json", "-c")
}

func main() {
	flag.Parse()

	logger := log.New(os.Stdout, "")
	file, err := os.Open(configFile)
	if err != nil {
		logger.Fatalf("failed to open config file: %s\n", err.Error())
	}
	defer file.Close()

	conf, err := config.FromJSON(file)
	if err != nil {
		logger.Fatalf("failed to open config file: %s\n", err.Error())
	}

	if err = cmd.NewApiCommand(logger, conf, buildOrdersService()).Execute(); err != nil {
		logger.Fatalf("failed to start api: %s\n", err.Error())
	}
}

func buildOrdersService() *ordersSvc.Service {
	return ordersSvc.NewService(
		inmemory.New(inmemory.SimpleIDGenerator()),
	)
}
