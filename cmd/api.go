package cmd

import (
	httpServeMux "applicationDesignTest/internal/api/http"
	"applicationDesignTest/internal/config"
	"applicationDesignTest/internal/log"
	"applicationDesignTest/internal/service/orders"
	"fmt"
	"net/http"
)

type apiCommand struct {
	logger        log.Logger
	conf          *config.Config
	ordersService *orders.Service
}

func NewApiCommand(logger log.Logger, conf *config.Config, ordersService *orders.Service) *apiCommand {
	return &apiCommand{
		logger:        logger,
		conf:          conf,
		ordersService: ordersService,
	}
}

func (cmd *apiCommand) Execute() error {
	port := fmt.Sprint(":", cmd.conf.Api.Http.Port)

	mux := httpServeMux.NewServeMux(cmd.logger, cmd.ordersService)
	mux.InitHandlers()

	cmd.logger.Infof("listening on %s\n", port)
	if err := http.ListenAndServe(port, mux); err != nil {
		return fmt.Errorf("error listening for server: %w", err)
	}

	return nil
}
